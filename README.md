# Tarefas de Hoje

## Overview

Hoje iremos usar o SNS como álibi para trabalhar com AWS EB, Docker, Dropwizard,
Metrics/Hawt e Papertrail.

## Começando:

Faça um fork em http://bitbucket.org/soaexpert-aws/soaexpert-sns para a sua conta, e depois faça o clone para a sua máquina local.

(Após o fork, mande o endereço para o seu querido instrutor)

Importe o projeto em seu IDE, e execute o arquivo Main com o argumento "server src/main/config/config.yml".

Em sua máquina, execute os seguintes comandos:

	[host]$ mvn clean package -Pawseb
	[host]$ vagrant up
	[guest]$ sudo usermod -a -G docker $USER

(reinicie a sessão)

	[guest]$ cd /vagrant/target/appassembler 
	[guest]$ docker build -t meuapp:latest .

Espere um pouco enquanto ele baixa a imagem. Para rodar localmente, digite:

	[guest]$ docker run -p 8080:8080 -i meuapp:latest

Feito isso, você pode testar em 8080 (guest) ou 9081 (host)

## Criando um processo com integração contínua na AWS

Acesse o codeship.io e crie uma conta no codeship.io. Clique em "Create a new Project", e conecte com seu projeto do bitbucket.

Selecione "Java and JVM-based languages", e preencha o seu "Setup Commands" como:

	mvn -U dependency:resolve

E deixe o seu "Test Commands como":

	mvn -B clean package -Pawseb

Clique em "Save and go to the Dashboard"

Conforme solicitado, configure o seu hook no seu projeto.

Agora modifique o seu pom.xml para refletir a sua conta, utilizando estes valores, porém alterando o conteúdo de meuLogin

	<meuLogin>aldrin-sns</meuLogin>
	<beanstalk.environmentName>${meuLogin}</beanstalk.environmentName>
	<beanstalk.cnamePrefix>${meuLogin}</beanstalk.cnamePrefix>
	<beanstalk.environmentRef>${meuLogin}.elasticbeanstalk.com</beanstalk.environmentRef>

Faça o commit e o push. Volte para o codeship e confirme que conseguiu um Green Build. 

Clique em "Set up Continuous Deployment". Clique em "Script" e preencha a configuração de "Deployment Commands":

	mvn -B -Pawseb package deploy beanstalk:put-environment

Clique no botão verde para confirmar. Na barra de navegação à direita, escolha "Environment" e preencha com o valor de AWS_ACCESS_KEY e AWS_SECRET_KEY da seguinte forma:

	AWS_ACCESS_KEY_ID=AKIAIDDHPR4LXFYWRLYA
	AWS_SECRET_ACCESS_KEY=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
	
Clique em "Save Changes".

Modifique o seu pom.xml para que o mesmo importe suas variáveis de ambiente, na seção de plugins em "build":

	<plugin>
		<groupId>br.com.ingenieux</groupId>
		<artifactId>beanstalk-maven-plugin</artifactId>
		<executions>
			<execution>
				<id>validate</id>
				<phase>validate</phase>
				<goals>
					<goal>expose-security-credentials</goal>
				</goals>
			</execution>
		</executions>
	</plugin>

Após isso, modifique no codeship e remova do seu "Deployment Commands" o comando beanstalk:put-environment.

## Configurando Logging

Crie uma conta no [papertrail](http://papertrailapp.com/) e entre nas configurações e obtenha o host e a porta em "Accounts | Log Destinations"

Modifique o seu config-aws.yml para refletir o host e a porta, e faça um push na sua cópia. 

Acompanhe o deployment no codeship e, após concluído, confirme se o papertrail registrou mensagens da sua aplicação.

## SNS

Crie a classe TesteSNSResource e declare um código mínimo, mas com o metered e o logging. 

	package com.soaexpert.sns.resources;
	
	import javax.ws.rs.Consumes;
	import javax.ws.rs.Path;
	import javax.ws.rs.Produces;
	
	import com.codahale.metrics.annotation.Metered;
	import com.fasterxml.jackson.databind.node.ObjectNode;
	
	@Path("/sns")
	@Consumes("*/*")
	@Produces("text/plain")
	public class TesteSNSResource extends BaseSNSResource {
		@Override
		@Metered
		public void handleNotification(String endpointId, ObjectNode bodyNode) {
			super.handleNotification(endpointId, bodyNode);
			
			logger.info("Nova mensagem para o endpoint {}: {}", endpointId, bodyNode);
		}
	
	}

Atualize sua aplicação e teste criando um tópico, uma assinatura e publicando uma mensagem para http://seuapp/sns/teste e conferindo no papertrail

## Monitoramento

Instale o hawt-crx 1.3.1. Basicamente, baixe o CRX (como daqui, por exemplo: http://mvnrepository.com/artifact/io.hawt/hawtio-crx/1.3.1) e abra o seu chrome e arraste o arquivo baixado.

Conecte o hawt em **seulogin**.elasticbeanstalk.com, porta 80, e investigue a aplicação remotamente.

