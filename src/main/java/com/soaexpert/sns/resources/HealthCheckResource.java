package com.soaexpert.sns.resources;

import com.codahale.metrics.health.HealthCheck;
import com.soaexpert.sns.healthcheck.AWSHealthCheck;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/health")
public class HealthCheckResource extends BaseResource {
    @Inject
    AWSHealthCheck awsHealthCheck;

    @GET
    @Path("/check")
    @Produces("application/json; charset=utf-8")
    public Response doCheck() {
        HealthCheck.Result execute = awsHealthCheck.execute();

        if (!execute.isHealthy()) {
            return Response.status(400).entity(execute).build();
        }

        return Response.ok().build();
    }
}
