package com.soaexpert.sns.resources;

import io.dropwizard.setup.Environment;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.codahale.metrics.Meter;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/sns")
@Consumes("*/*")
@Produces("text/plain")
public class TesteSNSResource extends BaseSNSResource {
	@Inject
	Environment env;
	
	@Override
	public void handleNotification(String endpointId, ObjectNode bodyNode) {
		Timer timer = env.metrics().timer(endpointId);
		
		Context ctx = timer.time();
		
		super.handleNotification(endpointId, bodyNode);
		
		ctx.stop();
		
		logger.info("Nova mensagem para o endpoint {}: {}", endpointId, bodyNode);
	}

}
